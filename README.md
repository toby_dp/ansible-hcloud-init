# ansible-hcloud-init

with this repo you can start to build up a new ansible project by using Hetzner cloud machines

# using
- start new servers in your project, include your ssh key inside
- add your hetzner hosts into the inventory file
- connect via ssh to the new hosts so that they are in your .ssh/known_hosts file
- run 'ansible-playbook /ansible/install_python.yml'
- run 'ansible-playbook /ansible/test.yml'
- start write your own ansible playbooks